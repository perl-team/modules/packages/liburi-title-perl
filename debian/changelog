liburi-title-perl (1.904-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.904.
  * Remove t/image.t from the list of skipped tests.
    It doesn't need internet access anymore.
  * Add new test dependencies for t/image.t.

 -- gregor herrmann <gregoa@debian.org>  Sun, 12 Feb 2023 17:44:28 +0100

liburi-title-perl (1.903-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.903.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Add libimage-png-libpng-perl | libimage-exiftool-perl to Recommends.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Feb 2023 19:23:11 +0100

liburi-title-perl (1.902-2) unstable; urgency=medium

  * Team upload.
  * Skip t/image.t during build and autopkgtest as it relies on internet
    access.
    Thanks to Niko Tyni for the analysis. (Closes: #962146)
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Thu, 04 Jun 2020 18:21:03 +0200

liburi-title-perl (1.902-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 1.902.
  * Skip the same tests during build and autopkgtest.
  * Update years of upstream copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Drop unneeded alternative build dependencies.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Avoid DNS resolution during tests.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Nov 2019 19:31:15 +0100

liburi-title-perl (1.901-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/rules: use 'wildcard' instead of 'shell echo'.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Jotam Jr. Trejo from Uploaders. Thanks for your work!

  [ Lucas Kanashiro ]
  * Import upstream version 1.901
  * Update debian/upstream/metadata
  * Update years of upstream copyright
  * debian/copyright: update Upstream-Contact
  * Update Debian packaging copyright
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.8
  * Add me as Uploader

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 12 Sep 2016 15:38:57 -0300

liburi-title-perl (1.900-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.900.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 May 2015 18:15:17 +0200

liburi-title-perl (1.89-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 1.89
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Oct 2014 21:49:16 +0200

liburi-title-perl (1.88-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update upstream maintainers / copyright holders.
  * Drop pod_syntax_fix.patch, fixed upstream.
  * Add (build) dependency on libmodule-pluggable-perl which will be
    removed from perl core.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 26 Feb 2014 19:23:17 +0100

liburi-title-perl (1.86-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ gregor herrmann ]
  * Skip test t/html.t during build. While it deals fine with a missing
    internet connection, it might fail if the retrieved webpages change.
    Thanks to Ansgar Burchardt for pointing out this issue.

 -- gregor herrmann <gregoa@debian.org>  Thu, 10 Jan 2013 17:14:56 +0100

liburi-title-perl (1.86-1) unstable; urgency=low

  * Initial Release. (Closes: #695951)

 -- Jotam Jr. Trejo <jotamjr@debian.org.sv>  Mon, 17 Dec 2012 13:50:23 -0600
